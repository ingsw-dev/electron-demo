export default {
  global: {
    font: {
      family: "MyriadPro",
      size: "14px",
      height: "20px"
    }
  },
  text: {
    color: "#4a4a48"
  }
};
