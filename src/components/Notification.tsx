import React from "react";
import { Box as BaseBox, Button } from "grommet";
import styled from "styled-components";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faExclamationTriangle,
  faInfoCircle
} from "@fortawesome/pro-light-svg-icons";

import Text from "./Text";

const Wrapper = styled(BaseBox)`
  border-radius: 4px;
  background: #ffffff;
  box-shadow: 0 5px 15px 0 rgba(0, 0, 0, 0.2);
  flex-direction: row;
  justify-content: space-between;
  height: 72px;
  align-items: center;
  margin-bottom: 40px;
`;

const Title = styled(Text)`
  font-weight: bold;
`;

type ButtonPairProps = {
  onAction: Function;
  onDismiss: Function;
  actionText?: string;
  dismissText?: string;
};

const ActionButton = styled(Button)`
  flex: 1;
  border-bottom: 1px solid #f3f3f3;
  padding: 0 11.5px;
  text-align: center;
  font-size: 10px;
  text-transform: uppercase;
  border-bottom-right-radius: 0;
  border-top-right-radius: 4px;
  &:last-child {
    border-bottom: none;
    border-bottom-right-radius: 4px;
    border-top-right-radius: 0;
  }
  &:hover {
    background: #4a4a48;
    color: #ffffff;
  }
`;

const ButtonPair: React.FC<ButtonPairProps> = ({
  onAction,
  actionText = "Read more",
  onDismiss,
  dismissText = "Dismiss"
}) => {
  return (
    <BaseBox
      height="72px"
      border={{ side: "left", size: "1px", style: "solid", color: "#f3f3f3" }}
    >
      <ActionButton
        onClick={() => {
          onAction();
        }}
      >
        {actionText}
      </ActionButton>
      <ActionButton
        onClick={() => {
          onDismiss();
        }}
      >
        {dismissText}
      </ActionButton>
    </BaseBox>
  );
};

type NotificationTypes = "alert" | "info";

const backgroundColors = {
  alert: "#ff0000",
  info: "#ffc95c"
};

const foregroundColors = {
  alert: "#ffffff",
  info: "#916000"
};

const icons = {
  alert: faExclamationTriangle,
  info: faInfoCircle
};

type NotificationBoxProps = {
  type: NotificationTypes;
};

const NotificationBox = styled(BaseBox)`
  height: 56px;
  width: 56px;
  align-items: center;
  justify-content: center;
  background: ${(props: NotificationBoxProps) => backgroundColors[props.type]};
  color: ${(props: NotificationBoxProps) => foregroundColors[props.type]};
  border-top-right-radius: 4px;
  border-bottom-right-radius: 4px;
  font-size: 24px;
`;

const TextContainer = styled(BaseBox)`
  flex: 1;
  margin: 0 24px;
`;

type Props = {
  type: NotificationTypes;
  title: string;
  text: string;
  onAction: Function;
  onDismiss: Function;
};

const Notification: React.FC<Props> = ({
  onAction,
  onDismiss,
  type,
  title,
  text
}) => {
  return (
    <Wrapper>
      <NotificationBox type={type}>
        <FontAwesomeIcon icon={icons[type]} />
      </NotificationBox>
      <TextContainer>
        <Title>{title}</Title>
        <Text>{text}</Text>
      </TextContainer>
      <ButtonPair onAction={onAction} onDismiss={onDismiss} />
    </Wrapper>
  );
};

export default Notification;
