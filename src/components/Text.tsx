import React from "react";
import styled from "styled-components";
import { Text as BaseText, TextProps } from "grommet";

type Props = {
  small?: boolean;
  selectable?: boolean;
  spaced?: boolean;
  color?: string;
  uppercase?: boolean;
};

const Text: React.FC<
  TextProps & Props & Omit<JSX.IntrinsicElements["span"], "color">
> = styled(BaseText)`
  cursor: ${(props: Props) => (props.selectable ? "text" : "default")};
  font-size: ${(props: Props) => (props.small ? "10px" : "14px")};
  line-height: normal;
  color: ${(props: Props) => (props.color ? props.color : "#4a4a48")};
  ${(props: Props) => props.spaced && "letter-spacing: 1px"}
  ${(props: Props) => props.uppercase && "text-transform: uppercase"}
`;

export default Text;
