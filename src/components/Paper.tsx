import React from "react";
import { Box, BoxProps } from "grommet";

type Props = BoxProps & JSX.IntrinsicElements["div"];

const Paper: React.FC<Props> = ({ style, children, ref, ...rest }) => {
  return (
    <Box
      style={{
        boxShadow: "0 1px 2px 0 rgba(0, 0, 0, 0.2)",
        borderRadius: "4px",
        ...style
      }}
      background="#ffffff"
      {...rest}
    >
      {children}
    </Box>
  );
};

export default Paper;
