import React from "react";
import { Box, Image } from "grommet";

const Logo = () => {
  return (
    <Box height="80px" direction="column" align="center" justify="center">
      <Image src="./astropay-logo.png" width="120px" />
    </Box>
  );
};

export default Logo;
