import React, { useState } from "react";
import { Box } from "grommet";
import styled from "styled-components";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faHome,
  faMoneyBillWave,
  faCoins,
  faUserTie,
  faEnvelopeOpen,
  faCog,
  faLifeRing,
  faUserEdit,
  IconDefinition
} from "@fortawesome/pro-light-svg-icons";
import { faCaretRight } from "@fortawesome/pro-solid-svg-icons";
import Text from "./Text";
import Avatar from "./Avatar";

type MenuProps = {
  bottom: any;
  active?: string;
  children: React.ReactElement[] | React.ReactElement;
};

const MenuWrapper = styled(Box)`
  width: 220px;
  height: 100vh;
  overflow: auto;
  border-right: 1px solid #e8e8e8;
  background: #ffffff;
  min-width: 220px;
`;

const Menu: React.FC<MenuProps> = ({ children, bottom, active }) => (
  <MenuWrapper direction="column">
    <Box flex={{ grow: 1 }}>
      {React.Children.map(children as React.ReactElement[], child => {
        return React.cloneElement(child, {
          ...child.props,
          active: active === child.props.name
        });
      })}
    </Box>
    {bottom}
  </MenuWrapper>
);

type TIcons =
  | "home"
  | "money"
  | "coins"
  | "user"
  | "messages"
  | "settings"
  | "help"
  | "profile";

type MenuItemProps = {
  icon: TIcons;
  children: React.ReactNode;
  active?: boolean;
  onClick?: Function;
  name: string;
};

type IconMappings = { [K in TIcons]: IconDefinition };

const iconMappings: IconMappings = {
  home: faHome,
  money: faMoneyBillWave,
  coins: faCoins,
  user: faUserTie,
  messages: faEnvelopeOpen,
  settings: faCog,
  help: faLifeRing,
  profile: faUserEdit
};

export const MenuItem: React.FC<MenuItemProps> = ({
  icon,
  active,
  children,
  onClick
}) => {
  const [hover, setHover] = useState(false);
  const turnOn = () => setHover(true);
  const turnOff = () => setHover(false);

  const color = active || hover ? "#4a4a48" : "#a4a4a3";
  return (
    <Box
      direction="row"
      height="55px"
      align="center"
      border={active && { side: "left", color: "#ff5c5c", size: "2px" }}
      style={{ cursor: "default" }}
      onMouseOver={turnOn}
      onMouseLeave={turnOff}
      onClick={() => {
        onClick && onClick();
      }}
    >
      <Box pad="11px">
        <Text size="20px">
          <FontAwesomeIcon icon={iconMappings[icon]} color={color} />
        </Text>
      </Box>
      <Box
        border={{ side: "top", color: "#f3f3f3" }}
        width="100%"
        height="100%"
        justify="center"
      >
        <Text color={color}>{children}</Text>
      </Box>
    </Box>
  );
};

type AccountProps = {
  name: string;
  picture?: string;
  company: string;
};

export const Account: React.FC<AccountProps> = ({ name, picture, company }) => {
  return (
    <Box direction="row" margin="12px 16px">
      <Avatar name={name} picture={picture} />
      <Box margin={{ left: "12px" }}>
        <Text>{name}</Text>
        <Text small spaced>
          {company}
        </Text>
      </Box>
      <Box align="center" justify="center" flex={{ grow: 1 }}>
        <FontAwesomeIcon icon={faCaretRight} color="#4a4a48" />
      </Box>
    </Box>
  );
};

export default Menu;
