import React from "react";
import { Image } from "grommet";

type Props = {
  country: "ar" | "br" | "en" | "mx";
};

const Flag: React.FC<Props> = ({ country }) => {
  return <Image src={`./flags/${country}.png`} />;
};

export default Flag;
