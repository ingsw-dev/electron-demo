import React, { Fragment, MouseEventHandler } from "react";
import { Box } from "grommet";
import Text from "./Text";

type TBreadcrumb = {
  title: string;
  onClick?: MouseEventHandler;
};

type BreadcrumbPops = {
  list: Array<TBreadcrumb>;
};

const Divider = () => (
  <Text margin="0 3px" size="16px">
    /
  </Text>
);

const Breadcrumbs: React.FC<BreadcrumbPops> = ({ list }) => {
  return (
    <Box
      background="#ffffff"
      direction="row"
      pad="18px 0 18px 28px"
      border={{ side: "bottom", size: "1px", color: "#d1d1d1" }}
      style={{ display: "block" }}
    >
      {list.map((breadcrumb, index) => {
        const isLast = index === list.length - 1;
        return (
          <Fragment key={breadcrumb.title}>
            <Text
              color={isLast ? "#4a4a48" : "#2799f0"}
              weight={isLast ? "bold" : "normal"}
              onClick={breadcrumb.onClick ? breadcrumb.onClick : () => {}}
            >
              {breadcrumb.title}
            </Text>
            {!isLast && <Divider />}
          </Fragment>
        );
      })}
    </Box>
  );
};

export default Breadcrumbs;
