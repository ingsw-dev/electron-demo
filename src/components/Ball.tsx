import styled from "styled-components";

const colors = {
  green: "#3bd5a9",
  yellow: "#ffc95c",
  red: "#ff5c5c"
};

type BallProps = JSX.IntrinsicElements["div"] & {
  color: "green" | "yellow" | "red";
};

const Ball = styled.span`
  width: 16px;
  height: 16px;
  border-radius: 8px;
  background-color: ${(props: BallProps) =>
    props.color ? colors[props.color] : colors["green"]};
`;

export default Ball;
