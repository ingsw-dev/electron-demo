import styled from "styled-components";

type Props = {
  color: "red" | "green" | "blue" | "black";
};

const colors = {
  red: "#ff5c5c",
  green: "#3bd5a9",
  blue: "#2799f0",
  black: "#4a4a48"
};

const Label = styled.label`
  border-radius: 12px;
  padding: 6px 28px;
  color: #f7f7f7;
  background-color: ${(props: Props) => colors[props.color]};
  font-size: 10px;
  text-transform: uppercase;
  line-height: initial;
`;

export default Label;
