import React from "react";
import { Box, Image as BaseImage, Text } from "grommet";
import styled from "styled-components";

type AvatarProps = {
  name: string;
  picture?: string;
};

const Image = styled(BaseImage)`
  width: 32px;
  border-top-left-radius: 4px;
  border-bottom-left-radius: 4px;
`;

const InitialsWrapper = styled(Box)`
  background: #3bcfd5;
  width: 32px;
  align-content: center;
  justify-content: center;
  text-align: center;
  border-top-right-radius: 4px;
  border-bottom-right-radius: 4px;
`;

const Avatar: React.FC<AvatarProps> = ({ name, picture }) => {
  return (
    <Box direction="row" height="32px">
      <Image src={picture} />
      <InitialsWrapper>
        <Text color="white">{name[0]}</Text>
      </InitialsWrapper>
    </Box>
  );
};

export default Avatar;
