import React, { ReactNode, ReactElement } from "react";
import styled from "styled-components";
import Paper from "./Paper";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEllipsisH } from "@fortawesome/pro-solid-svg-icons";
import { BoxProps, Box } from "grommet";
import Text from "../components/Text";

type PanelProps = {
  title?: string;
};
type Props = PanelProps & BoxProps & JSX.IntrinsicElements["div"];

const Panel: React.FC<Props> = ({ title, children, ...rest }) => {
  return (
    <Paper pad="24px" style={{ position: "relative" }} {...rest}>
      <FontAwesomeIcon
        icon={faEllipsisH}
        style={{
          position: "absolute",
          top: "24px",
          right: "24px",
          color: "#a4a4a3"
        }}
      />
      {title && <Title>{title}</Title>}
      {children}
    </Paper>
  );
};

type TablePanelProps = {
  title: string;
  titleRight?: ReactElement;
};

export const TablePanel: React.FC<TablePanelProps> = ({
  title,
  titleRight,
  children,
  ...rest
}) => {
  return (
    <Paper style={{ position: "relative" }} {...rest}>
      <Box pad="24px" direction="row" justify="between">
        <Title>{title}</Title>
        {titleRight}
      </Box>

      {children}
    </Paper>
  );
};

type PanelItem = {
  label: string;
  text?: string;
  rightContent?: ReactNode;
};

export const Item: React.FC<PanelItem> = ({ label, text, rightContent }) => {
  return (
    <Box
      pad="12.5px 0"
      border={text ? { side: "bottom", size: "1px", color: "#f3f3f3" } : false}
    >
      <Text small>{label}</Text>
      <Box direction="row" justify="between">
        {text && <Text selectable>{text}</Text>}
        {rightContent && rightContent}
      </Box>
    </Box>
  );
};

const TitleBox = styled(Box)`
  padding: 3px 6px;
  background: #f7f7f7;
  display: inline;
  max-width: max-content;
  border-radius: 16px;
  line-height: 10px;
  margin: 0 0 16px 0;
`;

export const Title: React.FC = ({ children }) => {
  return (
    <TitleBox>
      <Text
        small
        spaced
        style={{
          textTransform: "uppercase"
        }}
      >
        {children}
      </Text>
    </TitleBox>
  );
};

export default Panel;
