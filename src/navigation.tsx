import React, { useState, Fragment, ReactNode } from "react";

const NavigationContext = React.createContext(null);

const useNavigation: any = () => {
  const context = React.useContext(NavigationContext);
  if (!context) {
    throw new Error(`useNavigation must be used within a NavigationProvider`);
  }
  return context;
};

type NavigationProviderProps = {
  activeNavigation?: string;
};

const NavigationProvider: React.FC<NavigationProviderProps & any> = props => {
  const [activeNavigation, setNavigation] = useState(props.activeNavigation);
  const value = React.useMemo(() => [activeNavigation, setNavigation], [
    activeNavigation
  ]);
  return <NavigationContext.Provider value={value} {...props} />;
};

type SwitchProps = {
  children:
    | React.ReactElement<CaseProps>[]
    | React.ReactElement<CaseProps>
    | React.ReactElement<OtherwiseProps>;
};

const Switch: React.FC<SwitchProps> = ({ children }) => {
  const [active] = useNavigation();
  const switchedElement = React.Children.toArray(children as React.ReactElement<
    CaseProps | OtherwiseProps
  >[])
    .filter((child: any) => !!child.props.name)
    .filter((child: any) => child.props.name === active);
  const otherwiseElement = React.Children.toArray(
    children as React.ReactElement<CaseProps | OtherwiseProps>[]
  ).find((child: any) => child.type === Otherwise);
  const toRender =
    switchedElement.length > 0 ? switchedElement : otherwiseElement;
  return <Fragment>{toRender}</Fragment>;
};

type CaseProps = {
  children: ReactNode;
  name: string;
};

const Case: React.FC<CaseProps> = ({ children }) => {
  return <Fragment>{children}</Fragment>;
};

type OtherwiseProps = {
  children: ReactNode;
};

const Otherwise: React.FC<OtherwiseProps> = ({ children }) => {
  return <Fragment>{children}</Fragment>;
};

export { NavigationProvider, useNavigation, Switch, Case, Otherwise };
