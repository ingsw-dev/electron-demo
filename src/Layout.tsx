import React from "react";
import { Box } from "grommet";
import styled from "styled-components";

const BackgroundBox = styled(Box)`
  background: url("/background-pattern.png") repeat;
  background-size: inherit;
  min-height: 100vh;
  overflow: auto;
`;

const Layout: React.FC = ({ children }) => {
  return <BackgroundBox direction="row">{children}</BackgroundBox>;
};

const MainBox = styled.div`
  flex: 1;
  height: 100vh;
  overflow: auto;
`;

export const Main: React.FC = ({ children }) => {
  return <MainBox>{children}</MainBox>;
};

const InnerBox = styled.div`
  margin: 24px 106px;
`;

export const Inner: React.FC = ({ children }) => {
  return <InnerBox>{children}</InnerBox>;
};

export default Layout;
