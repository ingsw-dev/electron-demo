import React, { useState } from "react";
import { Box } from "grommet";
import styled from "styled-components";
import { TablePanel as Panel } from "../../components/Panel";
import Flag from "../../components/Flag";
import Label from "../../components/Label";
import Text from "../../components/Text";

const THead = styled.thead`
  border-top: 1px solid #e8e8e8;
  border-bottom: 1px solid #e8e8e8;
`;

const TBody = styled.tbody`
  background: #f3f3f3;
`;

const TD = styled.td`
  height: 56px;
  vertical-align: middle;
  text-align: center;
  color: #a4a4a3;
  font-size: 12px;
  border-right: 1px solid #e8e8e8;
  &:last-child {
    border-right: none;
  }
  width: 110px;
`;

const TR = styled.tr`
  border-bottom: 1px solid #e8e8e8;
`;

type TableProps = {
  allVisible: boolean;
};

const countries = ["ar", "br", "en", "mx"];
const statuses = {
  cleared: "green",
  pending: "blue",
  rejected: "red"
};

const Table: React.FC<TableProps> = ({ allVisible }) => {
  return (
    <table>
      <THead>
        <tr>
          <TD>Astropay ID</TD>
          <TD>Creation Date</TD>
          <TD>External ID</TD>
          <TD>Country</TD>
          <TD>Payment Method</TD>
          <TD>Last Change Date</TD>
          <TD>Currency</TD>
          <TD>Status</TD>
        </tr>
      </THead>
      <TBody>
        {Array.from(Array(allVisible ? 50 : 10).keys()).map(number => {
          let status = Object.keys(statuses)[Math.floor(Math.random() * 3)] as
            | "cleared"
            | "pending"
            | "rejected";

          return (
            <TR key={number}>
              <TD>{number + 1}</TD>
              <TD>
                00/00/0000
                <br />
                00:00:00
              </TD>
              <TD>000000</TD>
              <TD>
                <Flag
                  country={countries[Math.floor(Math.random() * 4)] as any}
                />
              </TD>
              <TD>000000</TD>
              <TD>
                00/00/0000
                <br />
                00:00:00
              </TD>
              <TD>000000</TD>
              <TD>
                {}
                <Label color={statuses[status] as "red" | "green" | "blue"}>
                  {status}
                </Label>
              </TD>
            </TR>
          );
        })}
      </TBody>
    </table>
  );
};

const TablePanel: React.FC = () => {
  const [allVisible, setAllVisible] = useState(false);
  const TableFilter = (
    <Box direction="row">
      <Text small>{allVisible ? "Showing 50" : "10 / 50"}</Text>
      <Text
        small
        color="#2799f0"
        margin="0 0 0 8px"
        style={{ cursor: "pointer" }}
        onClick={() => setAllVisible(!allVisible)}
      >
        View All
      </Text>
    </Box>
  );
  return (
    <Panel title="Client History" titleRight={TableFilter}>
      <Table allVisible={allVisible} />
    </Panel>
  );
};

export default TablePanel;
