import React, { Fragment } from "react";
import { Box } from "grommet";
import styled from "styled-components";
import BasePanel, { Item as PanelItem } from "../components/Panel";
import Label from "../components/Label";
import Ball from "../components/Ball";
import Flag from "../components/Flag";
import Text from "../components/Text";
import Notification from "../components/Notification";
import TablePanel from "./utils/TablePanel";

const Panel = styled(BasePanel)`
  flex: 1;
  margin-right: 17px;
  &:last-child {
    margin-right: 0;
  }
  min-height: 350px;
`;

const PanelContainer = styled(Box)`
  margin-bottom: 17px;
`;

const Divider = styled.span`
  border-right: 1px solid #f3f3f3;
  margin: -24px 24px;
`;

const ID = styled(Text)`
  font-size: 30px;
  line-height: 25px;
  margin-left: 8px;
`;

const Clients: React.FC = () => {
  return (
    <Fragment>
      <Notification
        text="This is an place holder notification."
        title="Important Notification"
        onAction={console.log}
        onDismiss={console.log}
        type="info"
      />
      <PanelContainer>
        <BasePanel>
          <Box direction="row">
            <Label color="black">Blacklist</Label>
            <Divider />
            <Box direction="row" align="center">
              <Text small>AP ID</Text>
              <ID>925375836</ID>
            </Box>
            <Divider />
          </Box>
        </BasePanel>
      </PanelContainer>
      <PanelContainer direction="row">
        <Panel title="Personal Information">
          <PanelItem label="Name" text="Julio Rios" />
          <PanelItem
            label="Country"
            text="Argentina"
            rightContent={
              <span>
                <Flag country="ar" />
              </span>
            }
          />
          <PanelItem label="Email" text="jrios@gmail.com" />
        </Panel>
        <Panel title="Status">
          <PanelItem label="Reason" text="Possible Fraud" />
          <PanelItem label="Blocked since" text="1/1/2019" />
        </Panel>
        <Panel title="Limits">
          <PanelItem label="User's deposit limits expressed in USD for Merchant." />
          <PanelItem label="Transaction" text="000000" />
          <PanelItem
            label="Daily"
            text="000000"
            rightContent={<Ball color="green" />}
          />
          <PanelItem
            label="Weekly"
            text="000000"
            rightContent={<Ball color="yellow" />}
          />
          <PanelItem
            label="Monthly"
            text="000000"
            rightContent={<Ball color="red" />}
          />
        </Panel>
      </PanelContainer>
      <PanelContainer>
        <TablePanel />
      </PanelContainer>
    </Fragment>
  );
};

export default Clients;
