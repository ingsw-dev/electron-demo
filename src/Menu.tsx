import React from "react";
import BaseMenu, { MenuItem, Account } from "./components/Menu";
import Logo from "./components/Logo";
import { useNavigation } from "./navigation";

const BottomComponent = (
  <Account
    name="Betboo"
    picture="https://api.adorable.io/avatars/400/abott@adorable.io.png"
    company="Astro Pay Direct"
  />
);

const Menu: React.FC = () => {
  const [active, setNavigation] = useNavigation();
  const onClick = (menuName: string) => () => setNavigation(menuName);
  return (
    <BaseMenu bottom={BottomComponent} active={active}>
      <Logo />
      <MenuItem onClick={onClick("home")} icon="home" name="home">
        Home
      </MenuItem>
      <MenuItem onClick={onClick("clients")} icon="user" name="clients">
        Clients
      </MenuItem>
      <MenuItem
        onClick={onClick("transactions")}
        icon="money"
        name="transactions"
      >
        Transactions
      </MenuItem>
      <MenuItem
        onClick={onClick("settlements")}
        icon="coins"
        name="settlements"
      >
        Settlements
      </MenuItem>
      <MenuItem onClick={onClick("messages")} icon="messages" name="messages">
        Messages
      </MenuItem>
      <MenuItem onClick={onClick("settings")} icon="settings" name="settings">
        Settings
      </MenuItem>
      <MenuItem onClick={onClick("help")} icon="help" name="help">
        Support
      </MenuItem>
      <MenuItem onClick={onClick("profile")} icon="profile" name="profile">
        Accounts
      </MenuItem>
    </BaseMenu>
  );
};

export default Menu;
