import React from "react";
import { Grommet } from "grommet";
import Breadcrumbs from "./components/Breadcrumbs";
import Notification from "./components/Notification";
import Layout, { Main, Inner } from "./Layout";
import Menu from "./Menu";
import { NavigationProvider, Switch, Case, Otherwise } from "./navigation";
import theme from "./theme";

import Clients from "./screens/Clients";

const App: React.FC = () => {
  return (
    <Grommet theme={theme}>
      <NavigationProvider activeNavigation="clients">
        <Layout>
          <Menu />
          <Main>
            <Switch>
              <Case name="clients">
                <Breadcrumbs
                  list={[
                    { title: "Clients", onClick: console.log },
                    { title: "Client Detail" }
                  ]}
                />
              </Case>
            </Switch>
            <Inner>
              <Switch>
                <Case name="clients">
                  <Clients />
                </Case>
                <Otherwise>
                  <Notification
                    text="The only working menu is Clients"
                    title="Important"
                    onAction={console.log}
                    onDismiss={console.log}
                    type="info"
                  />
                </Otherwise>
              </Switch>
            </Inner>
          </Main>
        </Layout>
      </NavigationProvider>
    </Grommet>
  );
};

export default App;
